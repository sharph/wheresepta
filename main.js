
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import haversine from 'haversine';
import bearing from '@turf/bearing';
import { point } from '@turf/helpers';
import _ from 'lodash';


var COMING_ANGLE = 65;
var DISTANCE_THRESHOLD = 2;
var NEAR_THRESHOLD = 0.3;
var VEHICLE_STOP_THRESHOLD = 0.3;
var STOP_DISTANCE_THRESHOLD = 0.2;
var REFRESH_SECONDS = 25;
var DETOUR_RELOAD_SECONDS = 60 * 10;
var DATA_WEIRDNESS_THRESHOLD = 40;
var DATA_WEIRDNESS_CLUES = _.sortBy(["MIDVALE", "FRONTIER", "SOUTHERN", "VICTORY BUS",
                                     "CALLOWHILL RAIL", "FRANKFORD", "FRANKFORD RAIL",
                                     "COMLY", "ALLEGHENY"]);

var SELECTABLE_STOPS = [
    {
        "n": "13th St Trolley Station",
        "r": ["11", "13", "34", "36", "10"],
        "x": -75.162559, "y": 39.952532
    },
    {
        "n": "15th St Trolley Station",
        "r": ["11", "13", "34", "36", "10"],
        "x": -75.165369, "y": 39.952502
    },
    {
        "n": "19th St Trolley Station",
        "r": ["11", "13", "34", "36", "10"],
        "x": -75.171483, "y": 39.953425
    },
    {
        "n": "22nd St Trolley Station",
        "r": ["11", "13", "34", "36", "10"],
        "x": -75.176571, "y": 39.954051
    },
    {
        "n": "30th St Trolley Station",
        "r": ["11", "13", "34", "36", "10"],
        "x": -75.1835, "y": 39.954815
    },
    {
        "n": "33rd St Trolley Station",
        "r": ["11", "13", "34", "36", "10"],
        "x": -75.1897, "y": 39.954773
    },
    {
        "n": "36th St Trolley Station",
        "r": ["11", "13", "34", "36"],
        "x": -75.194722, "y": 39.95389
    },
    {
        "n": "37th St Trolley Station",
        "r": ["11", "13", "34", "36"],
        "x": -75.196586, "y": 39.950993
    }
]

function milesFromObj(ref, septaObj) {
    return haversine(ref, {latitude: parseFloat(septaObj.lat),
                           longitude: parseFloat(septaObj.lng)},
                     {unit: "mile"});
}

function markRoutes(routes) {
    _.forEach(routes, (value, key) => {
        _.forEach(value, (route) => { route.route = key; });
    });
    return routes;
}

function App() {
    return (
        <div>
            <WhereSepta />
            <div id="footer-menu">
                <a href="https://gitlab.com/sharph/wheresepta">GitLab</a>
                <a href="https://sharphall.org/posts/2019/Sep/03/whereseptacom.html">Read me!</a>
            </div>
        </div>
    )
}

$(function () {
    ReactDOM.render(<App />,
                    document.getElementById('app'));
});


function Notice(props) {
    return (
        <div className="notice">{props.text}</div>
    );
}

function DetourData(props) {
    return (
        <div className="detour-data">
            <div className="detour-data-header">{props.title}</div>
            <div className="detour-data-value">{props.value}</div>
        </div>
    );
}

class Detour extends React.Component {
    constructor(props) {
        super(props);
        this.state = { opened: false }
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({ opened: !this.state.opened });
    }


    renderDetour(detourInfo) {
        var detourData = _.filter([
            {title: "Direction", value: detourInfo.route_direction},
            {title: "Reason", value: detourInfo.reason},
            {title: "Start Location", value: detourInfo.start_location},
            {title: "End Location", value: detourInfo.end_location},
            {title: "Start Date/Time", value: detourInfo.start_date_time},
            {title: "End Date/Time", value: detourInfo.end_date_time},
        ], x => (x.value !== ""));
        return (
            <div className="detour-info" key={detourInfo.current_message}>
                <div className="detour-message">
                    {detourInfo.current_message}
                </div>
                {detourData.map(detourData => (<DetourData title={detourData.title}
                                                           value={detourData.value}
                                                           key={detourData.title} />))}
            </div>
        );
    }

    render() {
        var className = "notice detour route-" + this.props.detour.route_id;
        return (
            <div className={className} onClick={this.toggle}>
                <div className="detour-header">
                    Route {this.props.detour.route_id} detour
                </div>
                {this.state.opened ?
                    this.props.detour.route_info.map(this.renderDetour)
                : <div>Tap to open.</div>}
            </div>
        );
    }
}

class WhereSepta extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            routeInfo: {},
            stops: [],
            vehicles: {},
            location: null,
            loaded: false,
            detours: [],
            dataWeirdness: 0,
            override: null
        };
        this.handleVisibilityChange = this.handleVisibilityChange.bind(this);
    }

    refresh() {
        if(this.updateTimer) {
            clearInterval(this.updateTimer);
        }
        if(this.detourTimer) {
            clearInterval(this.detourTimer);
        }
        this.pullNewData();
        this.loadDetours();
        this.updateTimer = setInterval(() => this.pullNewData(), REFRESH_SECONDS * 1000);
        this.detourTimer = setInterval(() => this.loadDetours(), DETOUR_RELOAD_SECONDS * 1000);
    }

    dataWeirdness(data) {
        return _.chain(data)
            .values()
            .filter(vehicles => _.findIndex(vehicles, vehicle => DATA_WEIRDNESS_CLUES.includes(vehicle.destination)) !== -1)
            .value().length;
    }

    pullNewData() {
        if (document.visibilityState == "visible") {
            $.ajax({
                url: "https://www3.septa.org/hackathon/TransitViewAll/",
                jsonp: "callback",
                dataType: "jsonp",
                success: (newData) => {
                    this.setState({
                        vehicles: markRoutes(newData.routes[0]),
                        loaded: true,
                        dataWeirdness: this.dataWeirdness(newData.routes[0])
                    });
                }
            });
        }
    }

    loadDetours() {
        $.ajax({
            url: "https://www3.septa.org/hackathon/BusDetours/",
            jsonp: "callback",
            dataType: "jsonp",
            success: (newData) => {
                this.setState({
                    detours: newData
                })
            }
        })
    }

    handleVisibilityChange() {
        if (document.visibilityState == "visible") { this.refresh() };
    }

    componentDidMount() {
        $.ajax({
            url: "stops.json",
            success: (stopData) => {
                // when we get a list of stops, also sort them by route for easy access
                var routes = _.map(stopData.routes, (route, key) => _.merge(route, {
                    stops: _.filter(stopData.stops, stop => stop.r.includes(key))
                }));
                this.setState({
                    routeInfo: stopData.routes,
                    stops: stopData.stops
                });
                console.log(stopData.stops);
            }
        });
        this.refresh();
        this.geolocationWatch = navigator.geolocation.watchPosition((position) => this.setState({
                location: {latitude: position.coords.latitude, longitude: position.coords.longitude}
            })
        );
        document.addEventListener("visibilitychange", this.handleVisibilityChange);
    }

    componentWillUnmount() {
        clearInterval(this.updateTimer);
        clearInterval(this.detourTimer);
        navigator.geolocation.clearWatch(this.geolocationWatch);
        document.removeEventListener("visibilitychange", this.handleVisibilityChange);
    }

    render() {
        var location = this.state.location;
        if (location === null) {
            return(
                <div>
                    <LocationSharingNotice />
                </div>
            );
        }
        if (!this.state.loaded) {
            return(<div className="notice">Please wait! Loading!</div>);
        }
        var here = point([location.longitude, location.latitude]);
        // find close stops
        var closestStops = _.chain(this.state.stops)
            .map(stop => {
                stop.distance = haversine(location, {latitude: stop.y,
                                                     longitude: stop.x});
                return stop;
            })
            .filter(stop => stop.distance < STOP_DISTANCE_THRESHOLD)
            .sortBy(stop => stop.distance)
            .value();

        var stopName = null;
        if(closestStops[0]) {
            stopName = closestStops[0].n;
        }

        if (this.state.override) {
            closestStops = [this.state.override];
            location = {longitude: this.state.override.x,
                        latitude: this.state.override.y};
            here = point([location.longitude, location.latitude]);
        }

        var selectedRoutes = _.chain(closestStops)
            .map(stop => stop.r)
            .flatten()
            .uniq()
            .value();


        var vehicles = _.chain(this.state.vehicles)
            .filter((routes, key) => selectedRoutes.includes(key))
            .values()
            .flatten()
            .map(vehicle => {
                vehicle.distance = milesFromObj(location, vehicle);
                var p = point([parseFloat(vehicle.lng), parseFloat(vehicle.lat)]);
                vehicle.bearing = bearing(p, here, {final: true});
                vehicle.alignment = (vehicle.heading - vehicle.bearing + 360) % 360;
                if (vehicle.distance < NEAR_THRESHOLD) {
                    vehicle.coming = vehicle.alignment < COMING_ANGLE || vehicle.alignment > 360 - COMING_ANGLE;
                    vehicle.going = vehicle.alignment > 180 - COMING_ANGLE && vehicle.alignment < 180 + COMING_ANGLE;
                } else {
                    vehicle.coming = vehicle.alignment < 90 || vehicle.alignment > 270;
                    vehicle.going = !vehicle.coming;
                }
                return vehicle;
            })
            .filter(vehicle => vehicle.distance < DISTANCE_THRESHOLD)
            .filter(vehicle => (vehicle.distance < NEAR_THRESHOLD || vehicle.coming))
            .sortBy(vehicle => vehicle.distance)
            .map(vehicle => {
                // select the closest stop for each vehicle if within threshold
                var selectedStop = null;
                vehicle.stopName = null;
                vehicle.distanceToStop = null;
                if (this.state.routeInfo[vehicle.route] !== undefined) {
                    selectedStop = _.chain(this.state.routeInfo[vehicle.route].stops)
                        .map(stop => {
                            stop = _.clone(stop);
                            stop.distance =  milesFromObj({latitude: stop.y,
                                                           longitude: stop.x},
                                                          vehicle);
                            return stop;
                        })
                        .filter(stop => stop.distance < VEHICLE_STOP_THRESHOLD)
                        .sortBy(stop => stop.distance)
                        .first().value();
                }
                if (selectedStop !== null && selectedStop !== undefined) {
                    vehicle.stopName = selectedStop.n;
                    vehicle.distanceToStop = selectedStop.distance;
                }
                return vehicle;
            })
            .value();
        var detours = _.filter(this.state.detours, detour => selectedRoutes.includes(detour.route_id));
        var notices = [];
        if (this.state.dataWeirdness >= DATA_WEIRDNESS_THRESHOLD) {
            notices.push("We think SEPTA data may be weird right now. " +
                         this.state.dataWeirdness + " routes included a vehicle with a destination of " +
                         DATA_WEIRDNESS_CLUES.join(', '));
        }
        return(
            <div>
                <LocationOverride onChange={loc => this.setState({ override: loc })}
                                  stopName={stopName} override={this.state.override} />
                <VehicleList notices={notices} detours={detours} vehicleList={vehicles} routeList={selectedRoutes} />
            </div>
        );
    }
}

class LocationOverride extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
    }

    setLocation(stop) {
        this.props.onChange(stop);
        this.setState({ open: false });
    }

    render() {
        var locationOptions = SELECTABLE_STOPS;
        if (this.state.open) {
            return (
                <div>
                    <div className="location-override head">Change Location</div>
                    {this.props.stopName ?
                        <div className="sub location-option" onClick={() => this.setLocation(null)}>
                            {this.props.stopName + ' '} (Device location)
                        </div>
                    :
                        <div className="sub location-option" onClick={() => this.setLocation(null)}>
                            Device location
                        </div>
                    }
                    {locationOptions.map(loc => (
                        <div className="sub location-option" onClick={() => this.setLocation(loc)} key={loc.n}>
                            {loc.n}
                        </div>
                    ))}
                </div>
            );
        }
        return (
            <div>
                <div className="location-override" onClick={() => this.setState({ open: true })}>
                    {(this.props.override ? this.props.override.n : this.props.stopName || 'Unknown') + ' '}
                    <span>(Change Location?)</span>
                </div>
            </div>
        );
    }
}

class Vehicle extends React.Component {
    static NICE_DIRECTIONS = {
        'NorthBound': 'going north',
        'SouthBound': 'going south',
        'WestBound': 'going west',
        'EastBound': 'going east',
    }


    render() {
        var distance;
        if (this.props.vehicleData.distance > 0.2) {
            distance = this.props.vehicleData.distance.toFixed(2) + " mi";
        } else {
            distance = (this.props.vehicleData.distance * 5280).toFixed(0) + " feet";
        }
        var nice_directions = Vehicle.NICE_DIRECTIONS[this.props.vehicleData.Direction];
        var className = "vehicle";
        var directionHint = "";
        if (this.props.vehicleData.coming) {
            className += " coming";
            directionHint = "incoming ";
        } else if (this.props.vehicleData.going) {
            className += " going";
        }
        className += " route-" + this.props.vehicleData.route;
        return (
            <div className={className}>
                <div className="route-identifier">{this.props.vehicleData.route}</div>
                { this.props.vehicleData.stopName ?
                    <div className="route-stop-name">{this.props.vehicleData.stopName + ' '}
                        <span className="route-distance">({distance})</span>
                    </div>
                    : <div className="route-distance">{directionHint}{distance} away</div>
                }
                <div className="route-directions">{nice_directions} towards {this.props.vehicleData.destination}</div>
                <div className="route-vehicle-id">{this.props.vehicleData.VehicleID}</div>
            </div>
        );
    }
}

class VehicleList extends React.Component {
    render() {
        var noVehicles = "You're near the SETPA routes listed above, " +
                         "but no vehicles for these routes were found " +
                         "within range.";
        var routeMarkers = _.map(this.props.routeList, routeName => ({
                routeName,
                className: "route-marker route-" + routeName
        }));
        return (
            <div>
                {routeMarkers.length > 0 ?
                    <div id="route-list">
                        {routeMarkers.map(routeMarker => (
                            <span className={routeMarker.className} key={routeMarker.routeName}>{routeMarker.routeName}</span>
                        ))}
                    </div>
                    :
                    <Notice text="You are not near any SEPTA stops." />
                }
                {this.props.notices.map(notice => <Notice text={notice} key={notice} />)}
                <div className="detours">
                {this.props.detours.map(detour => (
                    <Detour detour={detour} key={detour.route_id} />
                ))}
                </div>
                {this.props.vehicleList.length > 0 ?
                        <div>
                            {this.props.vehicleList.map(vehicle => (
                            <Vehicle vehicleData={vehicle} key={vehicle.VehicleID + vehicle.destination} />
                            ))}
                        </div>
                    :
                    <Notice text={noVehicles} />
                }
            </div>
        );
    }
}

class LocationSharingNotice extends React.Component {
    render() {
        return (
            <div className="locationSharingNotice">
                To see results, you must allow your device to share location
                information with this page. No location information will
                be transmitted over the internet.
            </div>
        );
    }
}
