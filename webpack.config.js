const CopyPlugin = require('copy-webpack-plugin');

module.exports = options => {
    return {
        entry: './main.js',
        output: {
            filename: 'bundle.js',
        },
        module: {
            rules: [
                {
                    test: /.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                cacheDirectory: true,
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new CopyPlugin([
                { from: 'index.html' },
                { from: 'style.css' },
                { from: 'stops.json' },
            ]),
        ]
    }
}
