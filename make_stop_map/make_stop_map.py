
import csv
import json


def reformat_stop(stop):
    return {
        "n": stop["stop_name"],
        "x": float(stop["stop_lon"]),
        "y": float(stop["stop_lat"]),
        "r": stop["routes"]
    }


def reformat_route(route):
    return {
        "n": route["route_long_name"]
    }


def main():
    routes = {}
    stops = {}
    trips = {}
    with open('google_bus/routes.txt', 'r') as f:
        for route in csv.DictReader(f):
            routes[route["route_id"]] = route
    with open('google_bus/stops.txt', 'r') as f:
        for stop in csv.DictReader(f):
            stops[stop["stop_id"]] = stop
            stops[stop["stop_id"]]["routes"] = []
    with open('google_bus/trips.txt', 'r') as f:
        for trip in csv.DictReader(f):
            trips[trip["trip_id"]] = trip
    with open('google_bus/stop_times.txt', 'r') as f:
        for stop_time in csv.DictReader(f):
            route_id = trips[stop_time["trip_id"]]["route_id"]
            stop = stops[stop_time["stop_id"]]
            if route_id not in stop["routes"]:
                stop["routes"].append(route_id)
    output = {
        "routes": dict([(k, reformat_route(route)) for k, route in routes.items()]),
        "stops": [reformat_stop(stop) for stop in stops.values()]
    }
    with open("stops.json", "w") as f:
        json.dump(output, f)


main()
