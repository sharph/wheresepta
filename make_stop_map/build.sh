#!/bin/bash

set -e

wget -q https://github.com/septadev/GTFS/releases/download/v202010243/gtfs_public.zip
unzip gtfs_public.zip
pushd google_bus
  unzip ../google_bus.zip
popd

PYTHONUNBUFFERED=1 python3 make_stop_map.py
