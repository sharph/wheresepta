FROM debian:stable AS DATA

RUN apt-get update && apt-get -y install python3 wget unzip
RUN mkdir /data
WORKDIR /data
COPY make_stop_map /data/
RUN ./build.sh

from node:14 as BUILD

ENV NODE_ENV=production

COPY . /app
WORKDIR /app

COPY --from=DATA /data/stops.json /app/stops.json

RUN npm install
RUN npm run build

FROM nginx:stable-alpine

RUN rm /usr/share/nginx/html/*
COPY --from=BUILD /app/dist/ /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
